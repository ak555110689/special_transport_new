import React, { useState, useEffect } from 'react';
import s from './Pagination.module.scss'

interface Item {
    id: number
    name: string
}

interface PaginationProps {
    items: Item[]
}

const Pagination: React.FC<PaginationProps> = ({ items, }) => {
    const [currentItems, setCurrentItems] = useState<Item[]>([]);

    useEffect(() => {
        setCurrentItems(items);
    }, [items]);

    return (
        <div className={s.disk}>
            <div className={s.displayFlex}>
                <h1 className={s.carsSearch}>Найдено { } Машин</h1>
            </div>
            <div >
                <div className={s.FiveP}>
                    <p>Направл</p>
                    <p>Транспорт</p>
                    <p>Вес/обьем</p>
                    <p>Машрут</p>
                    <p>Ставка</p>
                </div>
                <div className={s.ul}>
                    {currentItems.map(item => (
                        <div className={s.map} key={item.id}>{`${item.id}: ${item.name}`}</div>
                    ))}
                </div>
            </div>
        </div>

    );
};
export default Pagination;