import React, { FC } from 'react';
import s from './Card.module.scss'

const Card: FC = () => {
    return (
        <div className={s.card}>
            <h2>Enterprise</h2>
            <div className={s.money}>
                <p>$</p>
                <h3 className={s.price}>49</h3>
                <div className={s.description}>
                    <p>per user</p>
                    <p>per month</p>
                </div>
            </div>
            <p className={s.description}>All the features you need to keep your personal files safe, accessible, and easy to share.</p>
            <div className={s.advantages}> <h6>✓</h6>     <p>2 GB of hosting space</p></div>
            <div className={s.advantages}> <h6>✓</h6>     <p>14 days of free backups</p></div>
            <div className={s.advantages}> <h6>✓</h6>     <p>Social integrations</p></div>
            <h3 className={s.asdf}>Advanced client billing</h3>
            <button>Купить</button>
        </div>
    );
};

export default Card;