// type IObjectKeys = {
//     [key: string]: string | null | undefined
// }

import { File } from "react-pdf/dist/cjs/shared/types"

export type NewsData = {
	id: string
	title: string
	image: string
	text: string
}
export type CarsName = {
	id: string
	title: string
}
export type LoadingType = {
	city_country: string,
	address: string,
	time: string
}

export type CargoCreation = {
	type_cargo: string
	type_body: string
	weight: string
	weight_unit: string
	volume: string
	length: string
	width: string
	height: string
	readiness: string
	date: string
	work_day: string
	city_country: string
	address: string
	time_loading: string
	city_country_u: string
	address_u: string
	time_unloading: string
	body: string
	loading: string
	unloading: string
	payment: string
	note: string
	name: string
	phone_number: string
	price: string
	user: number | null
	image: File | null
	whatsapp: string
	telegram: string
	viber: string
	additionally: string
	loading_address1: null | LoadingType,
	loading_address2: null | LoadingType,
	loading_address3: null | LoadingType,
	loading_address4: null | LoadingType,
	loading_address5: null | LoadingType,
	unloading_address1: null | LoadingType,
	unloading_address2: null | LoadingType,
	unloading_address3: null | LoadingType,
	unloading_address4: null | LoadingType,
	unloading_address5: null | LoadingType,
}


export type HomePageCargo = {
	id: number
	type_cargo: string
	weight: number
	weight_unit: string
	volume: number
	length: number
	width: number
	height: number
	diameter: number
	readiness: string
	date: string
	work_day: string
	city_country: string
	address: string
	time_loading: string
	city_country_u: string
	address_u: string
	time_unloading: string
	body: string
	loading: string
	unloading: string
	loading_extra: string
	name: string
	phone_number: number
	user: number
	note: string
	price: string
	whatsapp: null | string
	telegram: null | string
	viber: null | string
	image: string
	created_at: string
	lifting_capacity: number
	from_where: string
	to_where: string
	type_body: string
	loading_address1: null | LoadingType,
	loading_address2: null | LoadingType,
	loading_address3: null | LoadingType,
	loading_address4: null | LoadingType,
	loading_address5: null | LoadingType,
	unloading_address1: null | LoadingType,
	unloading_address2: null | LoadingType,
	unloading_address3: null | LoadingType,
	unloading_address4: null | LoadingType,
	unloading_address5: null | LoadingType,
}

export type HomePageCars = {
	lifting_capacity: number
	from_where: string
	to_where: string
	address: string
	address_u: string
	body: string
	city_country: string
	city_country_u: string
	date: string
	diameter: number
	height: number
	id: number
	length: number
	loading: string
	loading_extra: string
	name: string
	phone_number: string
	price: string
	readiness: string
	time_loading: string
	time_unloading: string
	type_cargo: string
	type_body: string
	unloading: string
	user: number
	volume: number
	weight: number
	weight_unit: string
	width: number
	work_day: string
	created_at: string
	note: string
	whatsapp: null | string
	telegram: null | string
	viber: null | string

	images: string[]
}

export type CargoValidate = {
	country: string
	countryWhere: string
	from_where: string
	to_where: string
	region: string
	regionWhere: string
	city: string
	cityWhere: string
	lifting_capacity: string
	weight: string
	weightBefore: string
	volume: string
	volumeBefore: string
	date: string
	dateBefore: string
	Loading: string
	type: string
	typeCargoCars: string

}

export type UserData = {
	username: string
	password: string
	password2: string
	email: string
	phone: string
}

export type LocalUser = {
	username: string
	password: string
}

export type Comments = {
	id: number
	text: string
}

export type TruckUserData = {
	base_user: number
	id: number
	number_of_orders: number
	number_of_trucks: number
}

export type GetUserData = {
	city: string
	comments?: Comments[]
	email: string
	first_name: string
	id: number
	img: string
	country: string
	phone: string
	ratings?: null
	truck_user_data?: null | TruckUserData
	username: string
	tariff?: null
	average_rating: string
	whatsapp: string
	telegram: string
	viber: string
}

export type UserToken = {
	access: string
	refresh: string
	user: UserData | null
}

export type UserGetData = {
	username: string
	password: string
}

export type UserWithData = {
	username: string | undefined
	password: string | undefined
}

export type UserPassword = {
	password: string
	password2: string
}

export type DetailViewModulesTS = {
	id: number
	username: string
	email: string
	phone: string
	position: string
	country: string
	img: null
	city: string
	truck_user_data: []
	comments: CommentsDetail[]
	ratings: RatingsDetail[]
	average_rating: number
	tariff: null
	whatsapp: null | string
	telegram: null | string
	viber: null | string
}

export type DetailViewModules = {
	id: number
	username: string
	email: string
	phone: string
	position: string
	country: string
	img: string | null
	city: string
	truck_user_data: []
	comments: CommentsDetail[]
	ratings: RatingsDetail[]
	average_rating: number
	tariff: null
	whatsapp: null | string
	telegram: null | string
	viber: null | string
}
export type TruckUcerData = {
	id: number
	number_of_trucks: number
	number_of_orders: number
	base_user: number
}

export type CommentsDetail = {
	id?: number
	text: string
	user_id: number
}

export type AllCommentsDetail = {
	coment: CommentsDetail
	token: string
}

export type RatingsDetail = {
	id?: number
	rating: number
	user_id: number
	on_whom: number
}

export type AllRatings = {
	raiting: RatingsDetail
	token: string
}

export type OrderStatus = {
	id: number
	type_cargo: string
	city_country: string
	address: string
	city_country_u: string
	address_u: string
	date: string
	price: string
}
export type ChangeUserData = {
	username: string
	email: string
	phone: string
	city: string
	country: string
	img?: File | null
	whatsapp: string
	telegram: string
	viber: string
}

export type ForChangeUD = {
	token: string
	changeUD: ChangeUserData
}

export type CargoCarrier = {
	token: string
	id: number
}

export type BrandCar = {
	id: number
	title: string
}

export type AddDocument = {
	title: string
	user: number
	document: string | File
}

export type GetDocument = {
	document: string
	id: number
	title: string
	user: number
}

export type ForAddDocs = {
	addDocs: AddDocument
	token: string
}
export type AddDocsAuto = {
	Docs: AddDocument
	token: string
}
export type AddDocumentAuto = {
	truck: string
	docs: File
}
export type AddAllDocsAuto = {
	document: AddDocumentAuto
	token: string
}

// ======================================

export type DataForCargoCreation = {
	token: string
	cargoCreation: CargoCreation
}

export type CarCreation = {
	type_body: string
	type_cargo: string
	lifting_capacity: string
	volume: string
	length: string
	width: string
	height: string
	from_where: string
	to_where: string
	readiness: string
	work_day: string
	price: string
	user: number | null
	note: string
	date: string
	name: string
	phone_number?: string
	whatsapp: string
	telegram: string
	type_transport: string
	viber: string
	additionally: string
}

export type DataForCarCreation = {
	token: string
	carCreation: CarCreation
}

export type TruckDocs = {
	docs: string
	id: number
	truck: number
}

export type GetAutoPark = {
	brand: string
	brand_title: string
	model: string
	state_number: string
	truck_user: number
	type_truck: string
	id: number
	image: string
	truck_docs: null | TruckDocs[]
}

export type AddCar = {
	model: string
	state_number: string
	truck_user: number
	brand: number
	type_truck: string
	docs: File | string
	image: File | string
}

export type ForAddCar = {
	addCar: AddCar
	token: string
}

export type ErrorText = {
	all: string
	lifting_capacity: string
	volume: string
	weight: string
	length?: string,
	width?: string,
	height?: string,
}

export type DeletedDocument = {
	id: number
	token: string
}

export type EmailSend = {
	email: string
}

export type Message = {
	detail: string
	verification_code: string
}

export type ChangePassword = {
	email: string
	verification_code: string
	new_password: string
}
export type AddFull = {
	id: number
	image: string
	link: string
}