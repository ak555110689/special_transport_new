from django.contrib import admin
from django.urls import path, include, re_path
from django.conf import settings
from django.conf.urls.static import static
from rest_framework_simplejwt.views import TokenRefreshView

from .swagger import schema_view
from .health_check_api import HealthCheckAPI

urlpatterns = [
    # Token
    path('api/token/refresh/', TokenRefreshView.as_view(), name='token_refresh'),

    # healthcheck
    path('api/healthcheck/', HealthCheckAPI.as_view(), name='api-health-check'),

    # Admin
    path('admin/', admin.site.urls),

    # Auth
    path('accounts/', include('rest_framework.urls')),

    # Apps
    path('api/', include('apps.urls')),

    # Docs
    re_path(r'^swagger/$', schema_view.with_ui('swagger', cache_timeout=0), name='schema-swagger-ui'),
    re_path(r'^redoc/$', schema_view.with_ui('redoc', cache_timeout=0), name='schema-redoc'),
]

urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
