from django.urls import reverse
from rest_framework.test import APITestCase


class TestUserCRUD(APITestCase):
    USER_DATA = {
        "username": "user1234",
        "password": "Test1234__/",
        "password2": "Test1234__/",
        "email": "test@gmail.com",
        "phone": "+996500500500",
        "first_name": "string",
        "last_name": "string"
    }

    def test_user_registration(self):
        url = reverse('registration')
        context = self.client.post(url, data=self.USER_DATA)
        self.user_id = context.data['user']['id']
        self.access = context.data.get('access')
        assert context.status_code == 201
        assert 'access' in context.data
        assert 'refresh' in context.data

    def test_user_auth(self):
        self.test_user_registration()
        url = reverse('user_auth')
        data = {
            "username": "user1234",
            "password": "Test1234__/"
        }
        context = self.client.post(url, data=data)
        assert context.status_code == 201

    def test_user_update(self):
        self.test_user_registration()
        data = self.USER_DATA
        data['first_name'] = 'test_first_name'
        data['last_name'] = 'test_last_name'
        url = reverse('user-detail', args=[self.user_id])
        print(url)
        headers = {
            "Authorization": f"Bearer {self.access}"
        }
        context = self.client.patch(url, data=data, headers=headers)
        assert context.status_code == 200
        assert context.data['first_name'] == 'test_first_name'
        assert context.data['last_name'] == 'test_last_name'

    def test_user_delete(self):
        self.test_user_registration()

        # Удаляем пользователя
        url = reverse('user-detail', args=[self.user_id])
        headers = {
            "Authorization": f"Bearer {self.access}"
        }
        context = self.client.delete(url, headers=headers)
        assert context.status_code == 204

        # Пытаемся получить пользователя после удаления
        context = self.client.get(url)
        assert context.status_code == 404
