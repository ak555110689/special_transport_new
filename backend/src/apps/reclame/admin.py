from django.contrib import admin

from .models import UpReclame, DownReclame


@admin.register(UpReclame)
class UpReclame(admin.ModelAdmin):
    pass


@admin.register(DownReclame)
class DownReclame(admin.ModelAdmin):
    pass
