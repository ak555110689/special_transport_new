from django.urls import path, include
from rest_framework.routers import DefaultRouter

from .views import UpReclameView, DownReclameView


router = DefaultRouter()
router.register(r'up_reclames/', UpReclameView)
router.register(r'down_reclames/', DownReclameView)

urlpatterns = [
    path('', include(router.urls)),
]