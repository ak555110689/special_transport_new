from rest_framework import serializers

from .models import UpReclame, DownReclame


class UpReclameSerializer(serializers.ModelSerializer):

    class Meta:
        model = UpReclame
        fields = [
            'id', 'image', 'link'
        ]


class DownReclameSerializer(serializers.ModelSerializer):

    class Meta:
        model = DownReclame
        fields = [
            'id', 'image', 'link'
        ]
