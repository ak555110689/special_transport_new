from rest_framework import viewsets

from .models import UpReclame, DownReclame
from .serializers import UpReclameSerializer, DownReclameSerializer


class UpReclameView(viewsets.ReadOnlyModelViewSet):
    queryset = UpReclame.objects.all()
    serializer_class = UpReclameSerializer


class DownReclameView(viewsets.ReadOnlyModelViewSet):
    queryset = DownReclame.objects.all()
    serializer_class = DownReclameSerializer
