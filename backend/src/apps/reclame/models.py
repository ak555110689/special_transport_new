from django.db import models

class UpReclame(models.Model):
    image = models.ImageField(
        upload_to='up_reclame/',
        null=True, blank=True,
        verbose_name='Изображение'
    )
    link = models.URLField(
        null=True, blank=True,
        verbose_name='Ссылка'
    )

    def __str__(self) -> str:
        return f'{self.image}'
    
    class Meta:
        verbose_name = 'Верхняя реклама'
        verbose_name_plural = 'Верхнии рекламы'


class DownReclame(models.Model):
    image = models.ImageField(
        upload_to='down_reclame/',
        null=True, blank=True,
        verbose_name='Изображение'
    )
    link = models.URLField(
        null=True, blank=True,
        verbose_name='Ссылка'
    )

    def __str__(self) -> str:
        return f'{self.image}'
    
    class Meta:
        verbose_name = 'Нижняя реклама'
        verbose_name_plural = 'Нижнии рекламы'
