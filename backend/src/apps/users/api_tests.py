# from rest_framework import status
# from rest_framework.test import APITestCase
# from django.urls import reverse
# from django.contrib.auth import get_user_model
# from .models import TruckUser
#
# User = get_user_model()
#
#
# class AuthenticationTest(APITestCase):
#     def setUp(self):
#         self.user = User.objects.create_user(username='testuser', password='testpassword')
#         self.url = reverse('user_auth')
#
#     def test_authentication_success(self):
#         data = {
#             'username': 'testuser',
#             'password': 'testpassword'
#         }
#         response = self.client.post(self.url, data)
#         self.assertEqual(response.status_code, status.HTTP_201_CREATED)
#         self.assertTrue('refresh' in response.data)
#         self.assertTrue('access' in response.data)
#
#     def test_authentication_failure(self):
#         data = {
#             'username': 'testuser',
#             'password': 'wrongpassword'
#         }
#         response = self.client.post(self.url, data)
#         self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
#
#
# class RegistrationTest(APITestCase):
#     def setUp(self):
#         self.url = reverse('registration')
#
#     def test_registration_success(self):
#         data = {
#             "username": "newuser",
#             "password": "newpassword",
#             "password2": "newpassword",
#             "email": "newuser@example.com",
#             "phone": "1234567890",
#             "first_name": "FirstName",
#             "last_name": "LastName",
#             "city": "CityName",
#         }
#         response = self.client.post(self.url, data, format='multipart')
#         if response.status_code != status.HTTP_201_CREATED:
#             print(response.data)
#         self.assertEqual(response.status_code, status.HTTP_201_CREATED)
#         self.assertTrue('refresh' in response.data)
#         self.assertTrue('access' in response.data)
#
#     def test_registration_failure(self):
#         data = {
#             "username": "newuser",
#             "password": "newpassword",
#             "password2": "wrongpassword",
#             "email": "newuser@example.com",
#         }
#         response = self.client.post(self.url, data)
#         self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
