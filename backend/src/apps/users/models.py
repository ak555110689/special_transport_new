from django.contrib.auth import get_user_model
from django.contrib.auth.models import AbstractUser
from django.db import models

from ..base.services import path_img_user
from ..tariffs.models import Tariff


class BaseUser(AbstractUser):
    """Кастомная модель пользователя для хранения дополнительных данных.

    Добавленные поля:
    - phone (str): PhoneNumberField для хранения телефонных номеров пользователей.
    - img (file): ImageField для хранения изображений пользователей.
    - country (str): CharField для хранения информации о стране пользователя.
    - city (str): CharField для хранения информации о городе пользователя.
    - tariff (int): ForeignKey для хранения информации о тарифе клиента.
    """
    phone = models.CharField(
        max_length=17,
        verbose_name='Телефонный номер',
        help_text='Введите телефонный номер в международном формате.'
    )
    whatsapp = models.CharField(
        max_length=200, verbose_name='WhatsApp номер',
        null=True, blank=True
    )
    telegram = models.CharField(
        max_length=177, verbose_name='Телеграмм ссылка или номер',
        null=True, blank=True
    )
    viber = models.CharField(
        max_length=200, verbose_name='Viber номер',
        null=True, blank=True
    )
    img = models.ImageField(
        null=True,
        blank=True,
        upload_to=path_img_user,
        verbose_name='Изображение пользователя',
        help_text='Загрузите изображение пользователя.'
    )
    country = models.CharField(
        null=True, blank=True,
        max_length=100,
        verbose_name='Страна',
        help_text='Введите страну, в котором находится пользователь.'
    )
    city = models.CharField(
        null=True,
        blank=True,
        max_length=100,
        verbose_name='Город',
        help_text='Введите город, в котором находится пользователь.'
    )
    tariff = models.ForeignKey(
        Tariff,
        on_delete=models.PROTECT,
        null=True,
        blank=True,
        verbose_name='Тариф'
    )

    class Meta:
        app_label = 'users' # добавил app_label


class TruckUser(models.Model):
    """
    Класс `TruckUser` представляет пользователя с информацией о грузовых автомобилях.

    Атрибуты:
        base_user (BaseUser): Однозначное соответствие с моделью `BaseUser`.
        number_of_trucks (int): Количество грузовых автомобилей, связанных с пользователем.
        number_of_orders (int): Общее количество заказов.
    """
    base_user = models.OneToOneField(get_user_model(), on_delete=models.CASCADE, related_name='truck_user', verbose_name='id пользователя')
    number_of_trucks = models.PositiveIntegerField(verbose_name='Количество грузовых автомобилей', default=0)
    number_of_orders = models.PositiveIntegerField(verbose_name='Количество заказов', default=0)

    class Meta:
        verbose_name = 'Пользователь грузовика'
        verbose_name_plural = 'Пользователи грузовиков'

    def __str__(self):
        return str(self.base_user.username)
