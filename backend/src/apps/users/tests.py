# from django.test import TestCase
# from django.contrib.auth import get_user_model
# from .models import TruckUser
#
#
# User = get_user_model()
#
#
# class BaseUserModelTest(TestCase):
#     @classmethod
#     def setUpTestData(cls):
#         cls.user = User.objects.create_user(
#             username='testuser',
#             email='test@example.com',
#             phone='+1234567890',
#             city='TestCity',
#             img='backend/media/defaul_avatar.png'
#         )
#
#     def test_phone_label(self):
#         user = self.user
#         field_label = user._meta.get_field('phone').verbose_name
#         self.assertEquals(field_label, 'Телефонный номер')
#
#     def test_city_label(self):
#         user = self.user
#         field_label = user._meta.get_field('city').verbose_name
#         self.assertEquals(field_label, 'Город')
#
#     def test_user_str(self):
#         user = self.user
#         expected_user_name = user.username
#         self.assertEquals(expected_user_name, str(user))
#
#
# class TruckUserModelTest(TestCase):
#     @classmethod
#     def setUpTestData(cls):
#         cls.base_user = User.objects.create_user(
#             username='truckuser',
#             email='truckuser@example.com',
#             phone='+1234567890',
#             city='TruckCity',
#             img='backend/media/defaul_avatar.png'
#         )
#         cls.truck_user = TruckUser.objects.create(
#             base_user=cls.base_user,
#             number_of_trucks=5,
#             number_of_orders=10
#         )
#
#     def test_truck_user_link(self):
#         truck_user = self.truck_user
#         base_user = self.base_user
#         self.assertEquals(truck_user.base_user, base_user)
#
#     def test_number_of_trucks(self):
#         truck_user = self.truck_user
#         number_of_trucks = truck_user.number_of_trucks
#         self.assertEquals(number_of_trucks, 5)
#
#     def test_number_of_orders(self):
#         truck_user = self.truck_user
#         number_of_orders = truck_user.number_of_orders
#         self.assertEquals(number_of_orders, 10)
#
#     def test_truck_user_str(self):
#         truck_user = self.truck_user
#         expected_object_name = str(truck_user.base_user.pk)
#         self.assertEquals(expected_object_name, str(truck_user))