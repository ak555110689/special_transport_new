from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.utils.html import format_html

from .models import BaseUser, TruckUser


@admin.register(BaseUser)
class BaseUserAdmin(UserAdmin):
    list_display = ('username', 'phone', 'city', 'email', 'country', 'is_active', 'is_staff', 'img_avatar')
    search_fields = ('username', 'phone', 'city', 'email', 'country',)
    list_filter = ('is_active', 'is_staff', 'date_joined')

    fieldsets = (
        (None, {'fields': ('username', 'password')}),
        ('Personal Info', {'fields': ('email', 'phone', 'img', 'city', 'country',)}),
        ('Permissions', {'fields': ('is_active', 'is_staff', 'is_superuser', 'groups', 'user_permissions')}),
        ('Important dates', {'fields': ('last_login', 'date_joined')}),
    )

    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('username', 'password1', 'password2'),
        }),
    )

    def img_avatar(self, obj):
        return format_html('<img src="{}" width="100" height="100" />', obj.img.url) if obj.img else None
    img_avatar.short_description = 'Изображение'


@admin.register(TruckUser)
class TruckUserAdmin(admin.ModelAdmin):
    list_display = ('base_user', 'number_of_trucks', 'number_of_orders')
    list_filter = ('number_of_trucks', 'number_of_orders')
    search_fields = ('base_user__username', 'base_user__email')
    ordering = ('base_user__username',)

    fieldsets = (
        (None, {'fields': ('base_user',)}),
        ('Статистика', {'fields': ('number_of_trucks', 'number_of_orders')}),
    )

    readonly_fields = ('number_of_trucks', 'number_of_orders')
