from django.contrib.auth import get_user_model
from rest_framework import serializers
from rest_framework.validators import UniqueValidator
from django.db.models import Avg

from .models import TruckUser
from ..comments.serializers import SerializerComment
from ..ratings.serializers import SerializerRating

User = get_user_model()


class SerializerSetTruckUser(serializers.ModelSerializer):
    number_of_trucks = serializers.IntegerField(read_only=True)
    number_of_orders = serializers.IntegerField(read_only=True)

    class Meta:
        model = TruckUser
        fields = '__all__'


class SerializerSetBaseUser(serializers.ModelSerializer):
    username = serializers.CharField(
        required=False,
        validators=[
            UniqueValidator(queryset=User.objects.all())
        ]
    )
    email = serializers.EmailField(
        required=False,
        validators=[
            UniqueValidator(queryset=User.objects.all()),
        ]
    )
    password = serializers.CharField(
        write_only=True,
        required=False
    )
    img = serializers.ImageField(
        required=False,
        allow_null=True
    )
    truck_user_data = SerializerSetTruckUser(
        read_only=True,
        source='truck_user'
    )
    comments = SerializerComment(many=True, read_only=True, source='comment_id')
    ratings = SerializerRating(many=True, read_only=True, source='ratings_received')
    average_rating = serializers.SerializerMethodField()

    class Meta:
        model = User
        fields = [
            'id',
            'username',
            'email',
            'password',
            'phone',
            'whatsapp',
            'telegram',
            'viber',
            'country',
            'img',
            'city',
            'truck_user_data',
            'comments',
            'ratings',
            'average_rating',
            'tariff',
        ]

    def create(self, validated_data):
        password = validated_data.pop('password', None)
        instance = super().create(validated_data)

        if password:
            instance.set_password(password)
            instance.save()

        return instance

    def get_average_rating(self, obj):
        return obj.ratings_received.all().aggregate(Avg('rating'))['rating__avg']


class EmailVerificationSerializer(serializers.Serializer):
    email = serializers.EmailField()

class CodeVerificationSerializer(serializers.Serializer):
    email = serializers.EmailField()
    verification_code = serializers.CharField(max_length=4)
    new_password = serializers.CharField(min_length=8)
