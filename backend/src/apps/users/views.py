from django.contrib.auth import get_user_model
from django.contrib.auth.hashers import check_password
from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema
from rest_framework import viewsets, permissions, status, mixins
from rest_framework.decorators import api_view, permission_classes
from rest_framework.exceptions import ValidationError
from rest_framework.views import Response, APIView
from rest_framework_simplejwt.exceptions import TokenError
from rest_framework_simplejwt.tokens import RefreshToken
from rest_framework.permissions import IsAuthenticated
from rest_framework.parsers import MultiPartParser, FormParser

from .models import TruckUser
from .serializers import SerializerSetTruckUser, SerializerSetBaseUser, EmailVerificationSerializer, CodeVerificationSerializer
from .paginations import UserSetPagination
from .tasks import send_verification_code
import random
import string

User = get_user_model()


class SendVerifyCodeAPIView(APIView):
    serializer_class = EmailVerificationSerializer
    
    @swagger_auto_schema(request_body=EmailVerificationSerializer)
    def post(self, request):
        serializer = self.serializer_class(data=request.data)
        if serializer.is_valid():
            email = serializer.validated_data['email']
            user = User.objects.filter(email=email).first()
            if not user:
                raise ValidationError("Пользователь с таким email не найден")
            
            # Генерация случайного кода верификации
            verification_code = ''.join(random.choices(string.digits, k=4))
            
            # Отправка кода на электронную почту через Celery
            send_verification_code.delay(email, verification_code)
            
            return Response({"detail": "Код верификации отправлен успешно", "verification_code": verification_code})
        return Response(serializer.errors, status=400)

class ResetPasswordAPIView(APIView):
    serializer_class = CodeVerificationSerializer
    
    @swagger_auto_schema(request_body=CodeVerificationSerializer)
    def post(self, request):
        serializer = self.serializer_class(data=request.data)
        if serializer.is_valid():
            email = serializer.validated_data['email']
            verification_code = serializer.validated_data['verification_code']
            new_password = serializer.validated_data['new_password']
            
            # Проверка существования пользователя с указанным email
            user = User.objects.filter(email=email).first()
            if not user:
                raise ValidationError("Пользователь с таким email не найден")

            # # Проверка соответствия кода верификации, отправленного на почту,
            # # с кодом, введенным пользователем на фронте
            # if verification_code != provided_verification_code:
            #     raise ValidationError("Неверный код верификации")

            # Установка нового пароля
            user.set_password(new_password)
            user.save()
            
            return Response({"detail": "Пароль успешно изменен"})
        return Response(serializer.errors, status=400)


class UserViewSet(mixins.RetrieveModelMixin,
                  mixins.UpdateModelMixin,
                  mixins.ListModelMixin,
                  mixins.DestroyModelMixin,
                  viewsets.GenericViewSet):
    serializer_class = SerializerSetBaseUser
    queryset = User.objects.all().order_by('id')
    permission_classes = [permissions.IsAuthenticatedOrReadOnly]
    pagination_class = UserSetPagination

    @swagger_auto_schema(
        operation_description='Для изменения данных пользователя',
        request_body=openapi.Schema(
            type=openapi.TYPE_OBJECT,
            properties={
                "username": openapi.Schema(type=openapi.TYPE_STRING),
                "email": openapi.Schema(type=openapi.FORMAT_EMAIL),
                "phone": openapi.Schema(type=openapi.TYPE_STRING),
                'whatsapp': openapi.Schema(type=openapi.TYPE_STRING),
                'telegram': openapi.Schema(type=openapi.TYPE_STRING),
                'viber': openapi.Schema(type=openapi.TYPE_STRING),
                "country": openapi.Schema(type=openapi.TYPE_STRING),
                "img": openapi.Schema(type=openapi.TYPE_FILE),
                "city": openapi.Schema(type=openapi.TYPE_STRING),
            }
        ),
        responses={
            status.HTTP_201_CREATED: openapi.Response(
                description='User data and refresh, access',
                schema=openapi.Schema(
                    type=openapi.TYPE_OBJECT,
                    properties={
                        "id": openapi.Schema(type=openapi.TYPE_INTEGER),
                        "username": openapi.Schema(type=openapi.TYPE_STRING),
                        "email": openapi.Schema(type=openapi.FORMAT_EMAIL),
                        "phone": openapi.Schema(type=openapi.TYPE_STRING),
                        'whatsapp': openapi.Schema(type=openapi.TYPE_STRING),
                        'telegram': openapi.Schema(type=openapi.TYPE_STRING),
                        'viber': openapi.Schema(type=openapi.TYPE_STRING),
                        "country": openapi.Schema(type=openapi.TYPE_STRING),
                        "img": openapi.Schema(type=openapi.TYPE_STRING, format=openapi.FORMAT_URI),
                        "city": openapi.Schema(type=openapi.TYPE_STRING),
                        "truck_user_data": openapi.Schema(
                            type=openapi.TYPE_OBJECT,
                            description='Если пользователь не является водителем, значение будет null.',
                            properties={
                                "id": openapi.Schema(type=openapi.TYPE_INTEGER),
                                "number_of_trucks": openapi.Schema(type=openapi.TYPE_INTEGER),
                                "number_of_orders": openapi.Schema(type=openapi.TYPE_INTEGER),
                                "base_user": openapi.Schema(type=openapi.TYPE_INTEGER),
                            }
                        ),
                        "comments": openapi.Schema(
                            type=openapi.TYPE_OBJECT,
                            read_only=True,
                            properties={
                                "id": openapi.Schema(type=openapi.TYPE_INTEGER),
                                "text": openapi.Schema(type=openapi.TYPE_STRING),
                                "user_id": openapi.Schema(type=openapi.TYPE_INTEGER)
                            }
                        ),
                        "ratings": openapi.Schema(
                            type=openapi.TYPE_OBJECT,
                            read_only=True,
                            properties={
                                "id": openapi.Schema(type=openapi.TYPE_INTEGER),
                                "user_id": openapi.Schema(type=openapi.TYPE_INTEGER),
                                "rating": openapi.Schema(
                                    type=openapi.TYPE_INTEGER,
                                    maximum=5,
                                    minimum=0
                                )
                            }
                        ),
                        "tariff": openapi.Schema(type=openapi.TYPE_INTEGER)
                    }
                )
            ),
            status.HTTP_400_BAD_REQUEST: openapi.Response(
                description='Incorrect parameter entry'
            )
        },
    )
    def partial_update(self, request, *args, **kwargs):
        return super().partial_update(request, *args, **kwargs)

    @swagger_auto_schema(
        operation_description='Для изменения данных пользователя',
        request_body=openapi.Schema(
            type=openapi.TYPE_OBJECT,
            properties={
                "username": openapi.Schema(type=openapi.TYPE_STRING),
                "email": openapi.Schema(type=openapi.FORMAT_EMAIL),
                "phone": openapi.Schema(type=openapi.TYPE_STRING),
                "country": openapi.Schema(type=openapi.TYPE_STRING),
                "img": openapi.Schema(type=openapi.TYPE_FILE),
                "city": openapi.Schema(type=openapi.TYPE_STRING),
            }
        ),
        responses={
            status.HTTP_201_CREATED: openapi.Response(
                description='User data and refresh, access',
                schema=openapi.Schema(
                    type=openapi.TYPE_OBJECT,
                    properties={
                        "id": openapi.Schema(type=openapi.TYPE_INTEGER),
                        "username": openapi.Schema(type=openapi.TYPE_STRING),
                        "email": openapi.Schema(type=openapi.FORMAT_EMAIL),
                        "phone": openapi.Schema(type=openapi.TYPE_STRING),
                        'whatsapp': openapi.Schema(type=openapi.TYPE_STRING),
                        'telegram': openapi.Schema(type=openapi.TYPE_STRING),
                        'viber': openapi.Schema(type=openapi.TYPE_STRING),
                        "country": openapi.Schema(type=openapi.TYPE_STRING),
                        "img": openapi.Schema(type=openapi.TYPE_STRING, format=openapi.FORMAT_URI),
                        "city": openapi.Schema(type=openapi.TYPE_STRING),
                        "truck_user_data": openapi.Schema(
                            type=openapi.TYPE_OBJECT,
                            description='Если пользователь не является водителем, значение будет null.',
                            properties={
                                "id": openapi.Schema(type=openapi.TYPE_INTEGER),
                                "number_of_trucks": openapi.Schema(type=openapi.TYPE_INTEGER),
                                "number_of_orders": openapi.Schema(type=openapi.TYPE_INTEGER),
                                "base_user": openapi.Schema(type=openapi.TYPE_INTEGER),
                            }
                        ),
                        "comments": openapi.Schema(
                            type=openapi.TYPE_OBJECT,
                            read_only=True,
                            properties={
                                "id": openapi.Schema(type=openapi.TYPE_INTEGER),
                                "text": openapi.Schema(type=openapi.TYPE_STRING),
                                "user_id": openapi.Schema(type=openapi.TYPE_INTEGER)
                            }
                        ),
                        "ratings": openapi.Schema(
                            type=openapi.TYPE_OBJECT,
                            read_only=True,
                            properties={
                                "id": openapi.Schema(type=openapi.TYPE_INTEGER),
                                "user_id": openapi.Schema(type=openapi.TYPE_INTEGER),
                                "rating": openapi.Schema(
                                    type=openapi.TYPE_INTEGER,
                                    maximum=5,
                                    minimum=0
                                )
                            }
                        ),
                        "tariff": openapi.Schema(type=openapi.TYPE_INTEGER)
                    }
                )
            ),
            status.HTTP_400_BAD_REQUEST: openapi.Response(
                description='Incorrect parameter entry'
            )
        },
    )
    def update(self, request, *args, **kwargs):
        return super().update(request, *args, **kwargs)


class TruckUserViewSet(mixins.CreateModelMixin,
                       mixins.RetrieveModelMixin,
                       mixins.ListModelMixin,
                       viewsets.GenericViewSet):
    serializer_class = SerializerSetTruckUser
    queryset = TruckUser.objects.all()
    permission_classes = [permissions.IsAuthenticatedOrReadOnly]


@swagger_auto_schema(
    method='post',
    operation_description='Регистрация пользователя',
    request_body=openapi.Schema(
        type=openapi.TYPE_OBJECT,
        properties={
            "username": openapi.Schema(type=openapi.TYPE_STRING),
            'password': openapi.Schema(type=openapi.FORMAT_PASSWORD),
            'password2': openapi.Schema(type=openapi.FORMAT_PASSWORD),
            "email": openapi.Schema(type=openapi.FORMAT_EMAIL),
            "phone": openapi.Schema(type=openapi.TYPE_STRING)
        }
    ),
    responses={
        status.HTTP_201_CREATED: openapi.Response(
            description='User data and refresh, access',
            schema=openapi.Schema(
                type=openapi.TYPE_OBJECT,
                properties={
                    'user': openapi.Schema(
                        type=openapi.TYPE_OBJECT,
                        properties={
                            "id": openapi.Schema(type=openapi.TYPE_INTEGER),
                            "username": openapi.Schema(type=openapi.TYPE_STRING),
                            "email": openapi.Schema(type=openapi.FORMAT_EMAIL),
                            "phone": openapi.Schema(type=openapi.TYPE_STRING),
                            'whatsapp': openapi.Schema(type=openapi.TYPE_STRING),
                            'telegram': openapi.Schema(type=openapi.TYPE_STRING),
                            'viber': openapi.Schema(type=openapi.TYPE_STRING),
                            "truck_user_data": openapi.Schema(
                                type=openapi.TYPE_OBJECT,
                                description='Если пользователь не является водителем, значение будет null.',
                                properties={
                                    "id": openapi.Schema(type=openapi.TYPE_INTEGER),
                                    "number_of_trucks": openapi.Schema(type=openapi.TYPE_INTEGER),
                                    "number_of_orders": openapi.Schema(type=openapi.TYPE_INTEGER),
                                    "base_user": openapi.Schema(type=openapi.TYPE_INTEGER),
                                }
                            ),
                            "comments": openapi.Schema(
                                type=openapi.TYPE_OBJECT,
                                read_only=True,
                                properties={
                                    "id": openapi.Schema(type=openapi.TYPE_INTEGER),
                                    "text": openapi.Schema(type=openapi.TYPE_STRING),
                                    "user_id": openapi.Schema(type=openapi.TYPE_INTEGER)
                                }
                            ),
                            "ratings": openapi.Schema(
                                type=openapi.TYPE_OBJECT,
                                read_only=True,
                                properties={
                                    "id": openapi.Schema(type=openapi.TYPE_INTEGER),
                                    "user_id": openapi.Schema(type=openapi.TYPE_INTEGER),
                                    "rating": openapi.Schema(
                                        type=openapi.TYPE_INTEGER,
                                        maximum=5,
                                        minimum=0
                                    )
                                }
                            ),
                            "tariff": openapi.Schema(type=openapi.TYPE_INTEGER)
                        }
                    ),
                    'refresh': openapi.Schema(type=openapi.TYPE_STRING),
                    'access': openapi.Schema(type=openapi.TYPE_STRING),
                }
            )
        ),
        status.HTTP_400_BAD_REQUEST: openapi.Response(
            description='Incorrect parameter entry'
        )
    }
)
@api_view(['POST'])
@permission_classes([permissions.AllowAny])
def registration(request):
    serializer = SerializerSetBaseUser(data=request.data)
    serializer.is_valid(raise_exception=True)

    password = request.data.get('password')
    password2 = request.data.get('password2')
    if password and password != password2:
        raise ValidationError({"password": "Password fields didn't match."})

    user = serializer.save()
    refresh = RefreshToken.for_user(user)
    user_data = {
        'user': SerializerSetBaseUser(user).data,
        'refresh': str(refresh),
        'access': str(refresh.access_token),
    }

    return Response(user_data, status=201)


@swagger_auto_schema(
    method='post',
    operation_description='Авторизация пользователя',
    request_body=openapi.Schema(
        type=openapi.TYPE_OBJECT,
        properties={
            "username": openapi.Schema(type=openapi.TYPE_STRING),
            'password': openapi.Schema(type=openapi.FORMAT_PASSWORD),
        }
    ),
    responses={
        status.HTTP_201_CREATED: openapi.Response(
            description='User data and refresh, access',
            schema=openapi.Schema(
                type=openapi.TYPE_OBJECT,
                properties={
                    'user': openapi.Schema(
                        type=openapi.TYPE_OBJECT,
                        properties={
                            "id": openapi.Schema(type=openapi.TYPE_INTEGER),
                            "username": openapi.Schema(type=openapi.TYPE_STRING),
                            "email": openapi.Schema(type=openapi.FORMAT_EMAIL),
                            "phone": openapi.Schema(type=openapi.TYPE_STRING),
                            'whatsapp': openapi.Schema(type=openapi.TYPE_STRING),
                            'telegram': openapi.Schema(type=openapi.TYPE_STRING),
                            'viber': openapi.Schema(type=openapi.TYPE_STRING),
                            "truck_user_data": openapi.Schema(
                                type=openapi.TYPE_OBJECT,
                                description='Если пользователь не является водителем, значение будет null.',
                                properties={
                                    "id": openapi.Schema(type=openapi.TYPE_INTEGER),
                                    "number_of_trucks": openapi.Schema(type=openapi.TYPE_INTEGER),
                                    "number_of_orders": openapi.Schema(type=openapi.TYPE_INTEGER),
                                    "base_user": openapi.Schema(type=openapi.TYPE_INTEGER),
                                }
                            ),
                            "comments": openapi.Schema(
                                type=openapi.TYPE_OBJECT,
                                properties={
                                    "id": openapi.Schema(type=openapi.TYPE_INTEGER),
                                    "text": openapi.Schema(type=openapi.TYPE_STRING),
                                    "user_id": openapi.Schema(type=openapi.TYPE_INTEGER)
                                }
                            ),
                            "ratings": openapi.Schema(
                                type=openapi.TYPE_OBJECT,
                                read_only=True,
                                properties={
                                    "id": openapi.Schema(type=openapi.TYPE_INTEGER),
                                    "user_id": openapi.Schema(type=openapi.TYPE_INTEGER),
                                    "rating": openapi.Schema(
                                        type=openapi.TYPE_INTEGER,
                                        maximum=5,
                                        minimum=0
                                    )
                                }
                            ),
                            "tariff": openapi.Schema(type=openapi.TYPE_INTEGER)
                        }
                    ),
                    'refresh': openapi.Schema(type=openapi.TYPE_STRING),
                    'access': openapi.Schema(type=openapi.TYPE_STRING),
                }
            )
        ),
        status.HTTP_404_NOT_FOUND: openapi.Response(
            description='User not found'
        ),
        status.HTTP_401_UNAUTHORIZED: openapi.Response(
            description='Password incorrect'
        )
    }
)
@api_view(['POST'])
def authentication(request):
    username = request.data.get('username')
    password = request.data.get('password')

    try:
        user = User.objects.get(username=username)
    except User.DoesNotExist:
        return Response({'error': 'User not found'}, status=status.HTTP_404_NOT_FOUND)

    if not check_password(password, user.password):
        return Response({'error': 'Invalid password'}, status=status.HTTP_401_UNAUTHORIZED)

    refresh = RefreshToken.for_user(user)
    user_data = {
        'user': SerializerSetBaseUser(user).data,
        'refresh': str(refresh),
        'access': str(refresh.access_token),
    }

    return Response(user_data, status=201)


@swagger_auto_schema(
    method='post',
    operation_description='Добавляет refresh токен пользователя в черный список',
    request_body=openapi.Schema(
        type=openapi.TYPE_OBJECT,
        properties={
            "refresh": openapi.Schema(type=openapi.TYPE_STRING),
        }
    ),
    security=[{"Bearer": []}],
    responses={
        status.HTTP_205_RESET_CONTENT: openapi.Response(
            description='Successfully logged out',
            examples={'detail': 'Successfully logged out'}
        ),
        status.HTTP_400_BAD_REQUEST: openapi.Response(
            description='Token not valid',
            examples={'detail': 'Token not valid'}
        ),
    }
)
@api_view(['POST'])
@permission_classes([permissions.IsAuthenticated])
def logout_user(request):
    try:
        token = RefreshToken(request.data.get('refresh'))
        token.blacklist()
        return Response({'detail': 'Successfully logged out'}, status=205)
    except TokenError as e:
        return Response({'detail': 'Token not valid'}, status=400)


class UserProfile(APIView):
    permission_classes = [IsAuthenticated,]
    parser_classes = [MultiPartParser, FormParser]
    def get(self, request):
        user = self.request.user
        serializer = SerializerSetBaseUser(instance=user).data

        if 'img' in serializer:
            serializer['img'] = request.build_absolute_uri(serializer['img'])
            
        return Response(serializer)
    @swagger_auto_schema(
        manual_parameters=[
            openapi.Parameter('username', openapi.IN_FORM, type=openapi.TYPE_STRING),
            openapi.Parameter('email', openapi.IN_FORM, type=openapi.FORMAT_EMAIL),
            openapi.Parameter('phone', openapi.IN_FORM, type=openapi.TYPE_STRING),
            openapi.Parameter('whatsapp', openapi.IN_FORM, type=openapi.TYPE_STRING),
            openapi.Parameter('telegram', openapi.IN_FORM, type=openapi.TYPE_STRING),
            openapi.Parameter('viber', openapi.IN_FORM, type=openapi.TYPE_STRING),
            openapi.Parameter('city', openapi.IN_FORM, type=openapi.TYPE_STRING),
            openapi.Parameter('country', openapi.IN_FORM, type=openapi.TYPE_STRING),
            openapi.Parameter('img', openapi.IN_FORM, type=openapi.TYPE_FILE),
        ],
        responses={
            200: openapi.Schema(
                type=openapi.TYPE_OBJECT,
                properties={
                    'username': openapi.Schema(type=openapi.TYPE_STRING),
                    'email': openapi.Schema(type=openapi.FORMAT_EMAIL),
                    'phone': openapi.Schema(type=openapi.TYPE_STRING),
                    'whatsapp': openapi.Schema(type=openapi.TYPE_STRING),
                    'telegram': openapi.Schema(type=openapi.TYPE_STRING),
                    'viber': openapi.Schema(type=openapi.TYPE_STRING),
                    'city': openapi.Schema(type=openapi.TYPE_STRING),
                    "country": openapi.Schema(type=openapi.TYPE_STRING),
                    "img": openapi.Schema(type=openapi.TYPE_FILE),
                }
            ),
            400: openapi.Schema(
                type=openapi.TYPE_OBJECT,
                properties={
                    'field_errors': openapi.Schema(
                        type=openapi.TYPE_OBJECT,
                        properties={
                            'field_name': openapi.Schema(type=openapi.TYPE_STRING),
                            'error_message': openapi.Schema(type=openapi.TYPE_STRING)
                        }
                    ),
                }
            ),
        }
    )
    def put(self, request):
        user = self.request.user
        serializer = SerializerSetBaseUser(instance=user, data=request.data)

        if serializer.is_valid():
            # Проверяем, было ли отправлено новое изображение
            if 'img' not in request.data:
                # Если изображение не отправлено, оставляем существующее изображение без изменений
                serializer.validated_data['img'] = user.img
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)
        else:
            errors = serializer.errors
            field_errors = [{'field_name': field, 'error_message': error} for field, error in errors.items()]
            return Response({'field_errors': field_errors}, status=status.HTTP_400_BAD_REQUEST)
