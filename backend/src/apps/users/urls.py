from django.urls import path, include
from rest_framework.routers import DefaultRouter

from .views import (
    UserViewSet, TruckUserViewSet, UserProfile,
    registration, authentication, logout_user,
    SendVerifyCodeAPIView, ResetPasswordAPIView
)
router = DefaultRouter()
router.register(r'users', UserViewSet, basename='user')
router.register(r'truck_user', TruckUserViewSet, basename='truck_user')

urlpatterns = [
    path('registration/', registration, name='registration'),
    path('authentication/', authentication, name='user_auth'),
    path('logout/', logout_user, name='user_exit'),
    path('user/profile/', UserProfile.as_view(), name='user-profile'),
    path('send-code/', SendVerifyCodeAPIView.as_view(), name='send-code'),
    path('reset-password/', ResetPasswordAPIView.as_view(), name='reset-password'),
]

urlpatterns += router.urls
