from django.core.mail import send_mail
from django.conf import settings
from celery import shared_task


@shared_task
def send_verification_code(email, verification_code):
    # Отправка кода на электронную почту
    send_mail(
        'Код верификации для сброса пароля',
        f'Ваш код верификации для сброса пароля: {verification_code}',
        settings.EMAIL_HOST_USER,
        [email],
        fail_silently=False,
    )
