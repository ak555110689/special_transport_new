from rest_framework import viewsets
from rest_framework import permissions

from .models import News
from .serializers import SerializerSetNews
from .paginations import NewsSetPagination


class NewsViewSetReadOnly(viewsets.ReadOnlyModelViewSet):
    queryset = News.objects.all()
    serializer_class = SerializerSetNews
    permission_classes = [permissions.AllowAny]
    pagination_class = NewsSetPagination
