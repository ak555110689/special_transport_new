from django.contrib import admin
from django.utils.html import format_html

from .models import News


@admin.register(News)
class AdminNews(admin.ModelAdmin):
    list_display = ('short_title', 'short_text', 'image_preview')
    search_fields = ('title', 'text')
    ordering = ('title',)

    def short_title(self, obj):
        return f"{obj.title[:20]}..." if len(obj.title) > 20 else obj.title

    def short_text(self, obj):
        return f"{obj.text[:20]}..." if len(obj.text) > 20 else obj.text
    short_text.short_description = 'Текст (первые 50 символов)'

    def image_preview(self, obj):
        return format_html('<img src="{}" width="100" height="100" />', obj.image.url)
    image_preview.short_description = 'Изображение'

    readonly_fields = ("image_preview", )

    fieldsets = (
        (None, {'fields': ('title', 'image', 'text')}),
    )
