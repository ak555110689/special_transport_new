from django.urls import include, path
from rest_framework.routers import DefaultRouter

from .views import NewsViewSetReadOnly

router = DefaultRouter()
router.register(r'news', NewsViewSetReadOnly)

urlpatterns = [
    path('', include(router.urls)),
]
