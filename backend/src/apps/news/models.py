from django.db import models


class News(models.Model):
    """
    Модель новостей для хранения информации о новостных сообщениях.

    Атрибуты:
        title (str): Заголовок новости.
        image (ImageField): Изображение, связанное с новостью.
        text (str): Текст новости.
    """
    title = models.CharField(max_length=150)
    image = models.ImageField(upload_to='news/')
    text = models.TextField(max_length=2500)

    class Meta:
        verbose_name = "Новость"
        verbose_name_plural = "Новости"

    def __str__(self):
        return str(self.title[:20])
