from django.urls import path, include
from rest_framework.routers import DefaultRouter

from .views import TruckViewSet, BrandTruckViewReadOnly, MyDocumentView, TruckDocView

router = DefaultRouter()
router.register(r'trucks', TruckViewSet)
router.register(r'brand_trucks', BrandTruckViewReadOnly)
router.register(r'my_documents', MyDocumentView)
router.register(r'truck_docs', TruckDocView)

urlpatterns = [
    path('', include(router.urls)),
]
