from django.core.validators import FileExtensionValidator
from django.db import models

from ..users.models import TruckUser, BaseUser
from ..base.services import path_docs_truck


class BrandTruck(models.Model):
    """
    Модель представляющая бренд грузовика.

    Атрибуты:
        title (str): Название бренда грузовика.
    """
    title = models.CharField(max_length=150, unique=True, db_index=True, verbose_name='Название')

    class Meta:
        verbose_name = "Бренд грузовика"
        verbose_name_plural = "Бренды грузовиков"

    def __str__(self):
        return str(self.title)


class Truck(models.Model):
    """
    Модель представляющая грузовик.

    Атрибуты:
        truck_user (TruckUser): Владелец грузовика, связанный внешним ключом.
        brand (BrandTruck): Бренд грузовика, связанный внешним ключом.
        model (str): Модель грузовика. Максимальная длина 150 символов.
        type_truck (TypeTruck): Тип грузовика, связанный внешним ключом.
        state_number (str): Государственный номер грузовика. Максимальная длина 20 символов.
        image (ImageField): Изображение грузовика.

    Методы:
        - __str__: Возвращает строковое представление объекта, используя его первичный ключ.

    Мета-класс:
        - verbose_name: Название модели в единственном числе - 'Грузовик'.
        - verbose_name_plural: Название модели во множественном числе - 'Грузовики'.
    """
    truck_user = models.ForeignKey(TruckUser, on_delete=models.CASCADE, verbose_name="Владелец грузовика")
    brand = models.ForeignKey(
        BrandTruck, on_delete=models.PROTECT, 
        related_name='trucks' ,verbose_name="Бренд грузовика"
    )
    model = models.CharField(max_length=150, verbose_name="Модель грузовика")
    type_truck = models.CharField(max_length=150, verbose_name="Тип грузовика")
    state_number = models.CharField(max_length=20, verbose_name="Государственный номер")
    image = models.ImageField(
        upload_to='images/', verbose_name='Изображение грузовика',
        null=True, blank=True
        )

    class Meta:
        verbose_name = "Грузовик"
        verbose_name_plural = "Грузовики"

    def __str__(self):
        return str(self.pk)


class TruckDoc(models.Model):
    """
    Модель для хранения документов грузовика.

    Attributes:
        truck (Truck): Ссылка на грузовик.
        docs (FileField): Документы грузовика.

    Meta:
        verbose_name (str): Название модели в единственном числе.
        verbose_name_plural (str): Название модели во множественном числе.
    """
    truck =models.ForeignKey(
        Truck, on_delete=models.CASCADE,
        related_name='truck_docs', verbose_name='Грузовик'
    )
    docs = models.FileField(
        upload_to='docs/',
        validators=[
            FileExtensionValidator(
                allowed_extensions=[
                    'docx',
                    'pdf'
                ]
            )
        ],
        verbose_name="Документы грузовика",
        help_text='формат pdf или docx'
    )

    class Meta:
        verbose_name = 'Документ грузовика'
        verbose_name_plural = 'Документы грузовика'

    def __str__(self) -> str:
        return f'{self.truck}'

class MyDocument(models.Model):
    """
    Модель представляющая документ пользователя.

    Атрибуты:
        user (BaseUser): Пользователь, которому принадлежит документ, связанный внешним ключом.
        title (str): Название документа. Максимальная длина 100 символов.
        document (FileField): Файл документа, загружаемый в указанную директорию с использованием функции `path_docs`.
            Допустимые расширения: 'docx', 'pdf'.
    """
    user = models.ForeignKey(
        BaseUser, on_delete=models.CASCADE,
        related_name='my_documents', verbose_name='Пользователь'
    )
    title = models.CharField(
        max_length=100, verbose_name='Название документов',
        help_text='Введите название документа' 
    )
    document = models.FileField(
        upload_to='document/',
        validators=[
            FileExtensionValidator(
                allowed_extensions=[
                    'docx',
                    'pdf'
                ]
            )
        ],
        verbose_name="Документы грузовика",
        help_text='формат pdf или docx'
    )

    def __str__(self) -> str:
        return self.title
    
    class Meta:
        verbose_name = 'Мой документ'
        verbose_name_plural = 'Мои документы'
