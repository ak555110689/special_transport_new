from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema
from rest_framework import viewsets, status, filters
from rest_framework import permissions

from .models import Truck, BrandTruck, MyDocument, TruckDoc
from .serializers import SerializerTruck, SerializerBrandTruck, SerializerMyDocument, SerializerTruckDoc


class BrandTruckViewReadOnly(viewsets.ReadOnlyModelViewSet):
    """
    Для названий марок грузовых автомобилей
    """
    queryset = BrandTruck.objects.all()
    serializer_class = SerializerBrandTruck
    permission_classes = [permissions.AllowAny]
    pagination_class = None


class TruckDocView(viewsets.ModelViewSet):
    queryset = TruckDoc.objects.all()
    serializer_class = SerializerTruckDoc
    permission_classes = [permissions.IsAuthenticatedOrReadOnly]


class TruckViewSet(viewsets.ModelViewSet):
    """
    CRUD Truck
    """
    queryset = Truck.objects.all()
    serializer_class = SerializerTruck
    permission_classes = [permissions.IsAuthenticatedOrReadOnly]
    filter_backends = [filters.SearchFilter, filters.OrderingFilter]
    search_fields = ["truck_user__id"]
    ordering_fields = ["truck_user__base_user__username", "brand__title", "model", "type_truck", "state_number", "truck_user__id"]

    @swagger_auto_schema(
        type=openapi.TYPE_OBJECT,
        request_body=openapi.Schema(
            type=openapi.TYPE_OBJECT,
            properties={
                'model': openapi.Schema(type=openapi.TYPE_STRING, maxLength=150, minLength=1),
                'state_number': openapi.Schema(type=openapi.TYPE_STRING, maxLength=20, minLength=1),
                'truck_user': openapi.Schema(type=openapi.TYPE_INTEGER),
                'brand': openapi.Schema(type=openapi.TYPE_INTEGER),
                'type_truck': openapi.Schema(type=openapi.TYPE_STRING),
                'truck_docs': openapi.Schema(type=openapi.TYPE_FILE),
            }
        ),
        responses={
            status.HTTP_201_CREATED: openapi.Response(
                description='Description',
                schema=openapi.Schema(
                    type=openapi.TYPE_OBJECT,
                    properties={
                        'id': openapi.Schema(type=openapi.TYPE_INTEGER),
                        'model': openapi.Schema(type=openapi.TYPE_STRING, maxLength=150, minLength=1),
                        'state_number': openapi.Schema(type=openapi.TYPE_STRING, maxLength=20, minLength=1),
                        'truck_user': openapi.Schema(type=openapi.TYPE_INTEGER),
                        'brand': openapi.Schema(type=openapi.TYPE_INTEGER),
                        'type_truck': openapi.Schema(type=openapi.TYPE_STRING),
                        'truck_docs': openapi.Schema(type=openapi.TYPE_FILE, format=openapi.FORMAT_URI),
                    }
                )
            ),
        }
    )
    def create(self, request, *args, **kwargs):
        return super().create(request, *args, **kwargs)

    @swagger_auto_schema(
        type=openapi.TYPE_OBJECT,
        request_body=openapi.Schema(
            type=openapi.TYPE_OBJECT,
            properties={
                'model': openapi.Schema(type=openapi.TYPE_STRING, maxLength=150, minLength=1),
                'state_number': openapi.Schema(type=openapi.TYPE_STRING, maxLength=20, minLength=1),
                'truck_user': openapi.Schema(type=openapi.TYPE_INTEGER),
                'brand': openapi.Schema(type=openapi.TYPE_INTEGER),
                'type_truck': openapi.Schema(type=openapi.TYPE_STRING),
                'truck_docs': openapi.Schema(type=openapi.TYPE_FILE),
            }
        ),
        responses={
            status.HTTP_200_OK: openapi.Response(
                description='Description',
                schema=openapi.Schema(
                    type=openapi.TYPE_OBJECT,
                    properties={
                        'id': openapi.Schema(type=openapi.TYPE_INTEGER),
                        'model': openapi.Schema(type=openapi.TYPE_STRING, maxLength=150, minLength=1),
                        'state_number': openapi.Schema(type=openapi.TYPE_STRING, maxLength=20, minLength=1),
                        'truck_user': openapi.Schema(type=openapi.TYPE_INTEGER),
                        'brand': openapi.Schema(type=openapi.TYPE_INTEGER),
                        'type_truck': openapi.Schema(type=openapi.TYPE_STRING),
                        'truck_docs': openapi.Schema(type=openapi.TYPE_FILE, format=openapi.FORMAT_URI),
                    }
                )
            ),
        }
    )
    def partial_update(self, request, *args, **kwargs):
        return super().partial_update(request, *args, **kwargs)

    @swagger_auto_schema(
        type=openapi.TYPE_OBJECT,
        request_body=openapi.Schema(
            type=openapi.TYPE_OBJECT,
            properties={
                'model': openapi.Schema(type=openapi.TYPE_STRING, maxLength=150, minLength=1),
                'state_number': openapi.Schema(type=openapi.TYPE_STRING, maxLength=20, minLength=1),
                'truck_user': openapi.Schema(type=openapi.TYPE_INTEGER),
                'brand': openapi.Schema(type=openapi.TYPE_INTEGER),
                'type_truck': openapi.Schema(type=openapi.TYPE_STRING),
                'truck_docs': openapi.Schema(type=openapi.TYPE_FILE),
            }
        ),
        responses={
            status.HTTP_200_OK: openapi.Response(
                description='Description',
                schema=openapi.Schema(
                    type=openapi.TYPE_OBJECT,
                    properties={
                        'id': openapi.Schema(type=openapi.TYPE_INTEGER),
                        'model': openapi.Schema(type=openapi.TYPE_STRING, maxLength=150, minLength=1),
                        'state_number': openapi.Schema(type=openapi.TYPE_STRING, maxLength=20, minLength=1),
                        'truck_user': openapi.Schema(type=openapi.TYPE_INTEGER),
                        'brand': openapi.Schema(type=openapi.TYPE_INTEGER),
                        'type_truck': openapi.Schema(type=openapi.TYPE_STRING),
                        'truck_docs': openapi.Schema(type=openapi.TYPE_STRING, format=openapi.FORMAT_URI),
                    }
                )
            ),
        }
    )
    def update(self, request, *args, **kwargs):
        return super().update(request, *args, **kwargs)



class MyDocumentView(viewsets.ModelViewSet):
    queryset = MyDocument.objects.all()
    serializer_class = SerializerMyDocument
    filter_backends = [filters.SearchFilter,]
    search_fields = ["user__id"]
