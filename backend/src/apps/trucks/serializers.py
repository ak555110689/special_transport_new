from rest_framework import serializers

from .models import BrandTruck, Truck, MyDocument, TruckDoc


class SerializerTruckDoc(serializers.ModelSerializer):

    class Meta:
        model = TruckDoc
        fields = "__all__"


class SerializerBrandTruck(serializers.ModelSerializer):

    class Meta:
        model = BrandTruck
        fields = "__all__"


class SerializerTruck(serializers.ModelSerializer):
    docs = serializers.FileField(required=False)
    truck_docs = SerializerTruckDoc(many=True, read_only=True)
    brand_title = serializers.SerializerMethodField()

    class Meta:
        model = Truck
        fields = "__all__"
    
    def get_brand_title(self, obj):
        return obj.brand.title


class SerializerMyDocument(serializers.ModelSerializer):

    class Meta:
        model = MyDocument
        fields = "__all__"
