# Generated by Django 5.0.1 on 2024-03-18 18:03

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('trucks', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='truckdoc',
            name='truck_user',
        ),
    ]
