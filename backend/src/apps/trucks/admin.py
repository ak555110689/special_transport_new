from django.contrib import admin

from .models import BrandTruck, Truck, MyDocument, TruckDoc


class AdminTruckDoc(admin.TabularInline):
    model = TruckDoc
    extra = 1


@admin.register(BrandTruck)
class AdminBrandTruck(admin.ModelAdmin):
    list_display = ('title',)


@admin.register(Truck)
class AdminTypeTruck(admin.ModelAdmin):
    list_display = ('truck_user', 'brand', 'model', 'type_truck', 'state_number',)
    search_fields = ('model', 'state_number')
    list_filter = ('brand', 'type_truck')
    list_per_page = 20
    inlines = [AdminTruckDoc,]


@admin.register(MyDocument)
class AdminMyDocument(admin.ModelAdmin):
    list_display = ('title',)
    list_display_links = ('title',)
