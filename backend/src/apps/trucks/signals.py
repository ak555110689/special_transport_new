from django.db.models.signals import post_save, pre_delete
from django.dispatch import receiver

from .models import Truck
from ..users.models import TruckUser


@receiver(post_save, sender=Truck)
def truck_created(sender, instance, created, **kwargs):
    """
    Обработчик сигнала post_save для модели Truck.

    Увеличивает счетчик number_of_trucks у связанного объекта TruckUser при создании нового грузовика.
    """
    if created:
        try:
            truck_user = TruckUser.objects.get(pk=instance.truck_user.pk)
            truck_user.number_of_trucks += 1
            truck_user.save()
        except TruckUser.DoesNotExist:
            # Обработка случая, когда TruckUser с указанным PK не существует.
            pass


@receiver(pre_delete, sender=Truck, dispatch_uid="truck_deleted_handler")
def truck_deleted(sender, instance, **kwargs):
    """
    Обработчик сигнала pre_delete для модели Truck.

    Уменьшает счетчик number_of_trucks у связанного объекта TruckUser перед удалением грузовика.
    """
    try:
        truck_user = TruckUser.objects.get(pk=instance.truck_user.pk)
        truck_user.number_of_trucks -= 1
        truck_user.save()
    except TruckUser.DoesNotExist:
        # Обработка случая, когда TruckUser с указанным PK не существует.
        pass
