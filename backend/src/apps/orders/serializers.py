from rest_framework import serializers

from .models import AddCargo 


class AddCargoSerializer(serializers.ModelSerializer):

    class Meta:
        model = AddCargo
        fields = '__all__'


class SerilizerOrderHistory(serializers.ModelSerializer):
    class Meta:
        model = AddCargo
        fields = (
            'id', 'user', 'type_cargo', 'city_country',
            'address', 'city_country_u', 'address_u',
            'date', 'price',
        )
