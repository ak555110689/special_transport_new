from django.urls import path, include
from rest_framework.routers import DefaultRouter

from .views import AddCargoView, OrderHistoryView


router = DefaultRouter()
router.register(r'add_cargo', AddCargoView)

urlpatterns = [
    path('order_history/', OrderHistoryView.as_view(), name='order_history'),
    path('', include(router.urls)),
]
