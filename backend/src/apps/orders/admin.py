from django.contrib import admin

from .models import AddCargo


@admin.register(AddCargo)
class AdminAddCargo(admin.ModelAdmin):
    list_display = ('type_cargo',)
    list_display_links = ('type_cargo',)
    save_as = True
