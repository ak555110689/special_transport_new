from django.db import models

from apps.users.models import BaseUser


class AddCargo(models.Model):
    """
    Модель, представляющая информацию о добавленном грузе пользователем.

    Поля:
        - user: Пользователь, добавивший груз. Внешний ключ, связанный с моделью BaseUser.
        - type_cargo: Вид груза, представленный в виде текста (максимум 150 символов).
        - weight: Вес груза в выбранной единице измерения (см. WEIGHT_CARGO).
        - weight_unit: Единица измерения веса груза, выбираемая из определенного списка (см. WEIGHT_CARGO).
        - volume: Объем груза в кубических единицах.
        - length: Длина груза в метрах (валидаторы: от 0.01 до 15).
        - width: Ширина груза в метрах (валидаторы: от 0.01 до 4).
        - height: Высота груза в метрах (валидаторы: от 0.01 до 4).
        - diameter: Диаметр груза в метрах (валидаторы: от 0.01 до 10). Необязательное поле.

    Дополнительные поля:
        - readiness: Готовность груза к перевозке.
        - date: Дата создания заказа.
        - work_day: Дни работы для перевозки груза.

        Данные о месте загрузки:
        - city_country: Город/Страна загрузки.
        - address: Адрес загрузки.
        - time_loading: Время загрузки.

        Данные о месте выгрузки:
        - city_country_u: Город/Страна выгрузки.
        - address_u: Адрес выгрузки.
        - time_unloading: Дата и время разгрузки.

        Дополнительные параметры перевозки:
        - body: Тип кузова транспортного средства.
        - loading: Способ загрузки груза.
        - unloading: Способ разгрузки груза.
        - payment: Способ оплаты.
        - note: Примечание к заказу.
        - name: Имя контактного лица.
        - phone_number: Номер телефона контактного лица.
        - price: Стоимость перевозки.

    Методы:
        - __str__: Возвращает строковое представление объекта, используя вид груза.

    Мета-класс:
        - verbose_name: Название модели в единственном числе - 'Добавить груз'.
        - verbose_name_plural: Название модели во множественном числе - 'Добавлять грузы'.
    """
    user = models.ForeignKey(
        BaseUser, on_delete=models.CASCADE, 
        related_name='add_cargos', verbose_name='Пользователь'
    )
    body = models.CharField(
        max_length=150, verbose_name='Прицеп'
    )
    type_body = models.CharField(
        null=True, blank=True,
        max_length=150, verbose_name='Тип прицепа'
    )
    type_cargo = models.TextField(
        max_length=150, verbose_name='Вид груза'
    )
    weight = models.PositiveIntegerField(
        verbose_name='Вес груза'
    )
    weight_unit = models.CharField(
        max_length=150, verbose_name='Единица измерения'
    )
    volume = models.PositiveIntegerField(
        verbose_name='Объём груза(в куб. единиц)'
    )
    length = models.FloatField(
        verbose_name='Длина', null=True, blank=True
    )
    width = models.FloatField(
        verbose_name='Ширина', null=True, blank=True
    )
    height = models.FloatField(
        verbose_name='Высота', null=True, blank=True
    )

    readiness = models.CharField(
        max_length=150, verbose_name='Готовность'
    )
    date = models.DateField(
        null=True, blank=True, verbose_name='Дата'
    )
    work_day = models.CharField(
        max_length=150,
        null=True, blank=True, verbose_name='Дни работы'
    )

    city_country = models.TextField(
        verbose_name='Город/Страна загрузки'
    )
    address = models.TextField(
        verbose_name='Адрес загрузки',
        null=True, blank=True,
    )
    time_loading = models.TimeField(
        null=True, blank=True,
        verbose_name='Время загрузки'
    )
    loading_address1 = models.JSONField(
        null=True, blank=True,
        verbose_name='Адрес загрузки'
    )
    loading_address2 = models.JSONField(
        null=True, blank=True,
        verbose_name='Адрес загрузки'
    )
    loading_address3 = models.JSONField(
        null=True, blank=True,
        verbose_name='Адрес загрузки'
    )
    loading_address4 = models.JSONField(
        null=True, blank=True,
        verbose_name='Адрес загрузки'
    )
    loading_address5 = models.JSONField(
        null=True, blank=True,
        verbose_name='Адрес загрузки'
    )

    city_country_u = models.TextField(
        verbose_name='Город/Страна выгрузки'
    )
    address_u = models.TextField(
        verbose_name='Адрес выгрузки',
        null=True, blank=True,
    )
    time_unloading = models.TimeField(
        null=True, blank=True,
        verbose_name='Время разгрузки'
    )
    unloading_address1 = models.JSONField(
        null=True, blank=True,
        verbose_name='Адрес выгрузки'
    )
    unloading_address2 = models.JSONField(
        null=True, blank=True,
        verbose_name='Адрес выгрузки'
    )
    unloading_address3 = models.JSONField(
        null=True, blank=True,
        verbose_name='Адрес выгрузки'
    )
    unloading_address4 = models.JSONField(
        null=True, blank=True,
        verbose_name='Адрес выгрузки'
    )
    unloading_address5 = models.JSONField(
        null=True, blank=True,
        verbose_name='Адрес выгрузки'
    )

    loading = models.CharField(
        max_length=300, verbose_name='Загрузка',
        null=True, blank=True,
    )
    unloading = models.CharField(
        max_length=300, verbose_name='Разгрузка',
        null=True, blank=True,
    )
    payment = models.CharField(
        max_length=150,
        verbose_name = 'Оплата',
        null=True, blank=True,
    )
    note = models.TextField(
        null=True, blank=True, verbose_name='Примечание'
    )
    name = models.CharField(
        max_length=150, verbose_name='Имя',
        null=True, blank=True,
    )
    phone_number = models.CharField(
        max_length=13, verbose_name='Номер телефона',
        null=True, blank=True,
    )
    whatsapp = models.CharField(
        max_length=200, verbose_name='WhatsApp номер',
        null=True, blank=True
    )
    telegram = models.CharField(
        max_length=177, verbose_name='Телеграмм ссылка или номер',
        null=True, blank=True
    )
    viber = models.CharField(
        max_length=200, verbose_name='Viber номер',
        null=True, blank=True
    )
    price = models.CharField(
        max_length=150, verbose_name='Стоимость',
    )
    created_at = models.DateTimeField(
        auto_now_add=True, verbose_name='Дата создания заказа'
    )
    additionally = models.CharField(
        max_length=200,
        verbose_name='Дополнительно',
        null=True, blank=True
    )
    image = models.ImageField(
        upload_to='images/', verbose_name='Изображение',
        null=True, blank=True
    )

    def save(self, *args, **kwargs):
        if isinstance(self.price, (int, float)):
            self.price = str(self.price)
        super().save(*args, **kwargs)

    def __str__(self) -> str:
        return f'{self.type_cargo}'

    class Meta:
        verbose_name = 'Добавить груз'
        verbose_name_plural = 'Добавлять грузы'
