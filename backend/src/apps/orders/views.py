from rest_framework.viewsets import ModelViewSet
from rest_framework.permissions import IsAuthenticatedOrReadOnly
from django_filters import rest_framework as filters
from rest_framework.filters import SearchFilter
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import generics
from rest_framework.parsers import MultiPartParser, JSONParser


from .serializers import AddCargoSerializer, SerilizerOrderHistory
from .models import AddCargo


class AddCargoFilter(filters.FilterSet):
    weight_gt = filters.NumberFilter(field_name='weight', lookup_expr='gt')
    weight_lt = filters.NumberFilter(field_name='weight', lookup_expr='lt')
    type_cargo = filters.CharFilter(field_name='type_cargo', lookup_expr='icontains')
    volume_gt = filters.NumberFilter(field_name='volume', lookup_expr='gt')
    volume_lt = filters.NumberFilter(field_name='volume', lookup_expr='lt')
    city_country = filters.CharFilter(field_name='city_country', lookup_expr='icontains')
    city_country_u = filters.CharFilter(field_name='city_country_u', lookup_expr='icontains')
    date_gt = filters.DateFilter(field_name='date', lookup_expr='gt')
    date_lt = filters.DateFilter(field_name='date', lookup_expr='lt')

    class Meta:
        model = AddCargo
        fields = ['type_cargo', 'weight', 'volume', 'city_country', 'city_country_u', 'date',]


class AddCargoView(ModelViewSet):
    queryset = AddCargo.objects.all()
    serializer_class = AddCargoSerializer
    permission_classes = [IsAuthenticatedOrReadOnly,]
    filter_backends = [DjangoFilterBackend, SearchFilter,]
    filterset_class = AddCargoFilter
    search_fields = ('weight_gt', 'wheight_lt', 'volume_gt', 'volume_lt', 'type_cargo',)
    parser_classes = (MultiPartParser, JSONParser)


class OrderHistoryView(generics.ListAPIView):
    serializer_class = SerilizerOrderHistory
    filter_backends = [SearchFilter,]
    search_fields = ["user__id"]
    def get_queryset(self):
        user_id = self.request.query_params.get('user_id', None)
        if user_id is not None:
            queryset = AddCargo.objects.filter(user_id=user_id)
        else:
            queryset = AddCargo.objects.all()
        return queryset
