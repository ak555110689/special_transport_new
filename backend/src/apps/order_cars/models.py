from django.db import models

from django.core.validators import MaxValueValidator, MinValueValidator
from apps.users.models import BaseUser

class AddCar(models.Model):
    """
    Модель, представляющая информацию о добавленной машине.

    Поля:
        - user: Пользователь, добавивший машину. Внешний ключ, связанный с моделью BaseUser.
        - type_body: Тип кузова машины, представленный в виде строки (максимум 150 символов).
        - type_cargo: Тип погрузки машины,  представленный в виде строки (максимум 150 символов).
        - lifting_capacity: Грузоподъемность машины в тоннах (валидаторы: от 0 до 40).
        - volume: Объем груза в кубических единицах (валидаторы: от 0 до 70).
        - length: Длина машины в метрах (валидаторы: от 0.01 до 15).
        - width: Ширина машины в метрах (валидаторы: от 0.01 до 4).
        - height: Высота машины в метрах (валидаторы: от 0.01 до 4).

    Дополнительные поля:
        - from_where: Место отправления машины.
        - to_where: Место назначения машины.
        - readiness: Готовность машины к перевозке.
        - date: Дата создания заказа.
        - work_day: Дни работы для перевозки машины.
        - price: Стоимость перевозки.
        - note: Примечание к заказу.
        - name: Имя пользователя.
        - phone_number: Номер телефона пользователя.

    Методы:
        - __str__: Возвращает строковое представление объекта, используя тип кузова.

    Мета-класс:
        - verbose_name: Название модели в единственном числе - 'Добавить машину'.
        - verbose_name_plural: Название модели во множественном числе - 'Добавлять машины'.
    """
    user = models.ForeignKey(
        BaseUser, on_delete=models.CASCADE,
        related_name='add_cars', verbose_name='Пользователь'
    )
    type_transport = models.CharField(
        max_length=177, verbose_name='Тип транспорта',
        null=True, blank=True
    )
    type_body = models.CharField(
        max_length=150, verbose_name='Тип прицепа'
    )
    type_cargo = models.CharField(
        max_length=150, verbose_name='Тип погрузки'
    )
    lifting_capacity = models.PositiveIntegerField(
        validators=[MinValueValidator(0.2), MaxValueValidator(60)],
        verbose_name='Грузопоъемность(т)'
    )
    volume = models.PositiveIntegerField(
        validators=[MinValueValidator(1), MaxValueValidator(200)],
        verbose_name='Объём груза(в куб. единиц)',
    )
    length = models.FloatField(
        verbose_name='Длина', null=True, blank=True
    )
    width = models.FloatField(
        verbose_name='Ширина', null=True, blank=True
    )
    height = models.FloatField(
        verbose_name='Высота', null=True, blank=True
    )
    from_where = models.TextField(verbose_name='Откуда')
    to_where = models.TextField(verbose_name='Куда')
    readiness = models.CharField(
        max_length=150,
        verbose_name='Готовность',
        null=True, blank=True
    )
    date = models.DateField(
        null=True, blank=True, verbose_name='Дата'
    )
    work_day = models.CharField(
        max_length=150,
        null=True, blank=True,
        verbose_name='Дни работы'
    )
    price = models.CharField(
        max_length=150, verbose_name='Стоимость'
    )
    note = models.TextField(
        null=True, blank=True, verbose_name='Примечание')
    name = models.CharField(
        max_length=150, verbose_name='Имя',
        null=True, blank=True
    )
    phone_number = models.CharField(
        max_length=13, verbose_name='Номер телефона',
        null=True, blank=True
    )
    whatsapp = models.CharField(
        max_length=200, verbose_name='WhatsApp номер',
        null=True, blank=True
    )
    telegram = models.CharField(
        max_length=177, verbose_name='Телеграмм ссылка или номер',
        null=True, blank=True
    )
    viber = models.CharField(
        max_length=200, verbose_name='Viber номер',
        null=True, blank=True
    )
    additionally = models.CharField(
        max_length=200, 
        verbose_name='Дополнительно',
        null=True, blank=True
    )
    created_at = models.DateTimeField(
        auto_now_add=True, verbose_name='Дата создания'
    )

    def save(self, *args, **kwargs):
        if isinstance(self.price, (int, float)):
            self.price = str(self.price)
        super().save(*args, **kwargs)

    def __str__(self) -> str:
        return f'{self.type_body}'

    class Meta:
        verbose_name = 'Добавить машину'
        verbose_name_plural = 'Добавлять машины'
