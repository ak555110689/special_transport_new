from django.apps import AppConfig


class OrderCarsConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'apps.order_cars'
    verbose_name = 'Заказы машин'
