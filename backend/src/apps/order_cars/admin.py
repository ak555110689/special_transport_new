from django.contrib import admin

from .models import AddCar


@admin.register(AddCar)
class AddCarAdmin(admin.ModelAdmin):
    list_display = ('type_body',)
    list_display_links = ('type_body',)
