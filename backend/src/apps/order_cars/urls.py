from django.urls import path, include
from rest_framework.routers import DefaultRouter

from .views import AddCarView, OrderCarHistoryView

router = DefaultRouter()
router.register(r'add_car', AddCarView)

urlpatterns = [
    path('order_car_history/', OrderCarHistoryView.as_view(), name='order_car_history'),
    path('', include(router.urls)),
]
