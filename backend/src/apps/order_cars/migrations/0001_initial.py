# Generated by Django 3.2.7 on 2024-04-08 16:49

from django.conf import settings
import django.core.validators
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='AddCar',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('type_transport', models.CharField(max_length=177, verbose_name='Тип транспорта')),
                ('type_body', models.CharField(max_length=150, verbose_name='Тип прицепа')),
                ('type_cargo', models.CharField(max_length=150, verbose_name='Тип погрузки')),
                ('lifting_capacity', models.PositiveIntegerField(validators=[django.core.validators.MinValueValidator(0.2), django.core.validators.MaxValueValidator(60)], verbose_name='Грузопоъемность(т)')),
                ('volume', models.PositiveIntegerField(validators=[django.core.validators.MinValueValidator(1), django.core.validators.MaxValueValidator(200)], verbose_name='Объём груза(в куб. единиц)')),
                ('length', models.FloatField(blank=True, null=True, verbose_name='Длина')),
                ('width', models.FloatField(blank=True, null=True, verbose_name='Ширина')),
                ('height', models.FloatField(blank=True, null=True, verbose_name='Высота')),
                ('from_where', models.TextField(verbose_name='Откуда')),
                ('to_where', models.TextField(verbose_name='Куда')),
                ('readiness', models.CharField(max_length=150, verbose_name='Готовность')),
                ('date', models.DateField(blank=True, null=True, verbose_name='Дата')),
                ('work_day', models.CharField(blank=True, max_length=150, null=True, verbose_name='Дни работы')),
                ('price', models.CharField(max_length=150, verbose_name='Стоимость')),
                ('note', models.TextField(blank=True, null=True, verbose_name='Примечание')),
                ('name', models.CharField(max_length=150, verbose_name='Имя')),
                ('phone_number', models.CharField(max_length=13, verbose_name='Номер телефона')),
                ('whatsapp', models.CharField(blank=True, max_length=200, null=True, verbose_name='WhatsApp номер')),
                ('telegram', models.CharField(blank=True, max_length=177, null=True, verbose_name='Телеграмм ссылка или номер')),
                ('viber', models.CharField(blank=True, max_length=200, null=True, verbose_name='Viber номер')),
                ('additionally', models.CharField(blank=True, max_length=200, null=True, verbose_name='Дополнительно')),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='Дата создания')),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='add_cars', to=settings.AUTH_USER_MODEL, verbose_name='Пользователь')),
            ],
            options={
                'verbose_name': 'Добавить машину',
                'verbose_name_plural': 'Добавлять машины',
            },
        ),
    ]
