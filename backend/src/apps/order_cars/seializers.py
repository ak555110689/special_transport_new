from rest_framework import serializers
from .models import AddCar


class AddCarSerializer(serializers.ModelSerializer): 

    class Meta:
        model = AddCar
        fields = "__all__"

class OrderCarHistorySerializer(serializers.ModelSerializer):

    class Meta:
        model = AddCar
        fields = (
            'id', 'date', 
            'price', 'type_body',
        )
