from rest_framework.viewsets import ModelViewSet
from rest_framework.permissions import IsAuthenticatedOrReadOnly
from django_filters import rest_framework as filters
from rest_framework.filters import SearchFilter
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework.response import Response
from rest_framework import status
from rest_framework.views import APIView

from .models import AddCar
from .seializers import AddCarSerializer, OrderCarHistorySerializer


class AddCarFilter(filters.FilterSet):
    lifting_capacity_gt = filters.NumberFilter(field_name='lifting_capacity', lookup_expr='gt')
    lifting_capacity_lt = filters.NumberFilter(field_name='lifting_capacity', lookup_expr='lt')
    volume_gt = filters.NumberFilter(field_name='volume', lookup_expr='gt')
    volume_lt = filters.NumberFilter(field_name='volume', lookup_expr='lt')
    type_body = filters.CharFilter(field_name='type_body', lookup_expr='icontains')
    type_cargo = filters.CharFilter(field_name='type_cargo', lookup_expr='icontains')
    from_where = filters.CharFilter(field_name='from_where', lookup_expr='icontains')
    to_where = filters.CharFilter(field_name='to_where', lookup_expr='icontains')
    date_gt = filters.DateFilter(field_name='date', lookup_expr='gt')
    date_lt = filters.DateFilter(field_name='date', lookup_expr='lt')

    class Meta:
        model = AddCar
        fields = ['lifting_capacity', 'volume', 'type_body', 'type_cargo', 'from_where', 'to_where', 'date']


class AddCarView(ModelViewSet):
    queryset = AddCar.objects.all()
    serializer_class = AddCarSerializer
    permission_classes = [IsAuthenticatedOrReadOnly,]
    filter_backends = [DjangoFilterBackend, SearchFilter]
    filterset_class = AddCarFilter
    search_fields = ('lifting_capacity_gt', 'lifting_capacity_lt', 'volume_gt', 'volume_lt', 'type_body', 'type_cargo')


class OrderCarHistoryView(APIView):
    def get(self, request):
        car_history = AddCar.objects.all()
        serializer = OrderCarHistorySerializer(car_history, many=True)
        return Response(serializer.data)