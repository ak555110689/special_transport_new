from django.contrib import admin

from .models import Comment


@admin.register(Comment)
class AdminComment(admin.ModelAdmin):
    list_display = ('id', 'display_username', 'short_text')
    search_fields = ('id', 'user_id__username', 'text')

    def display_username(self, obj):
        return obj.user_id.username

    display_username.short_description = 'Username'

    def short_text(self, obj):
        return (obj.text[:20] + '...') if len(obj.text) > 20 else obj.text

    short_text.short_description = 'Short Text'
