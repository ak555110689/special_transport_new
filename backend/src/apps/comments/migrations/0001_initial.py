# Generated by Django 5.0.1 on 2024-03-07 17:40

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Comment',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('text', models.CharField(max_length=300, verbose_name='Содержание')),
            ],
            options={
                'verbose_name': 'комментарий',
                'verbose_name_plural': 'Комментарий',
            },
        ),
    ]
