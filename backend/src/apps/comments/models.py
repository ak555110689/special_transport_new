from django.contrib.auth import get_user_model
from django.db import models


class Comment(models.Model):
    """
    Модель представляющая комментарий.

    Атрибуты:
        user_id (User): Пользователь, связанный внешним ключом.
        text (str): Содержание комментария.
    """
    user_id = models.ForeignKey(get_user_model(), on_delete=models.CASCADE, related_name='comment_id', verbose_name='Пользователь')
    text = models.CharField(max_length=300, verbose_name='Содержание')

    class Meta:
        verbose_name = 'комментарий'
        verbose_name_plural = 'Комментарий'

    def __str__(self):
        return str(self.user_id.username)
