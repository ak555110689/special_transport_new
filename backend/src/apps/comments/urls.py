from django.urls import path, include
from rest_framework.routers import DefaultRouter

from .views import ViewCommentSet

router = DefaultRouter()
router.register(r'comments', ViewCommentSet)

urlpatterns = [
    path('', include(router.urls)),
]
