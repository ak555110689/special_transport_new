from rest_framework import viewsets, permissions, mixins

from .serializers import SerializerComment
from .models import Comment


class ViewCommentSet(mixins.ListModelMixin,
                     mixins.CreateModelMixin,
                     mixins.RetrieveModelMixin,
                     mixins.DestroyModelMixin,
                     viewsets.GenericViewSet):
    queryset = Comment.objects.all()
    serializer_class = SerializerComment
    permission_classes = [permissions.IsAuthenticatedOrReadOnly]
