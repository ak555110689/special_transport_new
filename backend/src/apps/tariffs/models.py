from django.db import models


class Tariff(models.Model):
    """
    Модель представляющая тариф.

    Атрибуты:
        title (str): Название тарифа. Максимальная длина 150 символов.
        price (int): Цена тарифа, положительное целое число.
        description (str): Описание тарифа. Максимальная длина 500 символов.
    """
    title = models.CharField(max_length=150, verbose_name="Название тарифа")
    price = models.PositiveIntegerField(verbose_name="Цена тарифа")
    description = models.TextField(max_length=500, verbose_name="Описание тарифа")

    def __str__(self):
        return str(self.title)

    class Meta:
        verbose_name = "Тариф"
        verbose_name_plural = "Тарифы"
