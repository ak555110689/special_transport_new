from rest_framework import viewsets, permissions

from .models import Tariff
from .serializers import SerializerModelTariff


class ViewListTariff(viewsets.ReadOnlyModelViewSet):
    queryset = Tariff.objects.all()
    serializer_class = SerializerModelTariff
    permission_classes = [permissions.AllowAny]
