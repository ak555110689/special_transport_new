from django.contrib import admin

from .models import Tariff


@admin.register(Tariff)
class AdminTariff(admin.ModelAdmin):
    pass
