from django.urls import include, path
from rest_framework.routers import DefaultRouter

from .views import ViewListTariff

router = DefaultRouter()
router.register(r'tariffs', ViewListTariff)


urlpatterns = [
    path('', include(router.urls)),
]
