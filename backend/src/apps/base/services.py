

def path_img_user(instance, file) -> str:
    """
    Путь к аватару пользователя
    """
    return f"user_{instance.pk}/img/{file}"


def path_docs_truck(instance, file) -> str:
    """
    Функция генерирует путь для сохранения документов грузовика
    """
    return f"user_{instance.truck_user.base_user.pk}/docx/{file}"
