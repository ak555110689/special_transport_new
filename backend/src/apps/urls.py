from django.urls import path, include


urlpatterns = [
    # User
    path('user/', include('apps.users.urls')),

    # News
    path('news/', include('apps.news.urls')),

    # Truck
    path('truck/', include('apps.trucks.urls')),

    # Comments
    path('comment/', include('apps.comments.urls')),

    # Rating
    path('rating/', include('apps.ratings.urls')),

    # Orders
    path('order/', include('apps.orders.urls')),

    # Tariff
    path('tariff/', include('apps.tariffs.urls')),

    # Order_cars
    path('order_cars/', include('apps.order_cars.urls')),

    # Reclame
    path('reclame/', include('apps.reclame.urls')),
]
