from django.contrib import admin

from .models import Rating


@admin.register(Rating)
class AdminRating(admin.ModelAdmin):
    search_fields = ('id', 'user_id', "rating")
