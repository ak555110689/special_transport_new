from django.urls import path, include
from rest_framework.routers import DefaultRouter

from .views import RatingSetViews

router = DefaultRouter()
router.register('ratings', RatingSetViews)

urlpatterns = [
    path('', include(router.urls)),
]
