from django.contrib.auth import get_user_model
from django.core.validators import MaxValueValidator
from django.db import models


class Rating(models.Model):
    """
    Модель представляющая рейтинг пользователя.

    Атрибуты:
        user_id (User): Пользователь, связанный один к одному внешним ключом.
        rating (int): Рейтинг пользователя, положительное целое число не превышающее 5.
        on_whom (User): Пользователь, на которого оставлена оценка, связанный внешним ключом.
    """
    user_id = models.ForeignKey(
        get_user_model(),
        on_delete=models.CASCADE,
        verbose_name="Пользователь",
        related_name='ratings_given'
    )
    rating = models.PositiveIntegerField(
        validators=[
            MaxValueValidator(5)
        ],
        verbose_name="Рейтинг"
    )
    on_whom = models.ForeignKey(
        get_user_model(),
        on_delete=models.CASCADE,
        verbose_name='Пользовательская оценка',
        related_name='ratings_received'
    )

    def __str__(self):
        return f"{self.user_id.username}: {self.rating}"

    class Meta:
        verbose_name = "Рейтинг"
        verbose_name_plural = "Рейтинги"
