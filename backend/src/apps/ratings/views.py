from rest_framework import viewsets, permissions

from .models import Rating
from .serializers import SerializerRating


class RatingSetViews(viewsets.ModelViewSet):
    queryset = Rating.objects.all()
    serializer_class = SerializerRating
    permission_classes = [permissions.IsAuthenticatedOrReadOnly]
