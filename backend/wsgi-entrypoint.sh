#!/bin/sh

until cd /app
do
  echo 'Waiting'
done

echo "Checking for src/.env file"
sleep 2
if [ -e src/.env ]; then
    echo "src/.env file already exists"
else
    cp src/.env.example src/.env
    echo "Created a default .env file"
fi
sleep 2

until ./src/manage.py migrate
do
  echo 'Waiting DB'
  sleep 2
done

./src/manage.py collectstatic --noinput

gunicorn config.wsgi --bind 0.0.0.0:8080 --workers 4 --threads 4